#include <mauve/runtime.hpp>
#include <mauve/tracing.hpp>
#include "benchmarking/mauve_component.hpp"

namespace benchmarking {

using CpuBurn = mauve::runtime::Component<mauve::runtime::Shell,
    CpuBurnCore, CpuBurnFSM>;

struct ReconfigurationArchitecture;

struct CpuBurnReconfCore: public CpuBurnCore {
  void setArchitecture(ReconfigurationArchitecture* a) { this->architecture = a; };

  virtual bool configure_hook() override;

  virtual void update() override;

  void reconfigure();

private:
  int count;
  int reconf;
  ReconfigurationArchitecture* architecture;
};

struct ReconfigurationArchitecture: public mauve::runtime::Architecture {
  CpuBurn& A = mk_component<CpuBurn>("/A");
  CpuBurn& B = mk_empty_component<CpuBurn>("/B");

  virtual bool configure_hook() override {
    B.make<mauve::runtime::Shell, CpuBurnReconfCore, CpuBurnFSM>();
    dynamic_cast<CpuBurnReconfCore*>(& B.core())->setArchitecture(this);

    A.fsm().sleep.set_clock(mauve::runtime::ms_to_ns(100));
    B.fsm().sleep.set_clock(mauve::runtime::ms_to_ns(100));
    A.set_cpu(1);
    B.set_cpu(1);
    A.set_priority(20);
    B.set_priority(30);
    A.core().C = 0.02;
    B.core().C = 0.01;
    A.core().config_time = 0.08;
    A.configure();
    B.configure();
    return true;
  }
};

bool CpuBurnReconfCore::configure_hook() {
  count = 0;
  reconf = 30;
  return CpuBurnCore::configure_hook();
}

void CpuBurnReconfCore::update() {
  if (count < reconf) {
    count++;
    CpuBurnCore::update();
  }
  else {
    reconfigure();
    count = 0;
  }
}

void CpuBurnReconfCore::reconfigure() {
  mauve::tracing::trace_start("reconfiguration");
  auto clock = architecture->A.get_time();
  bool r;
  r = architecture->A.get_task().stop();
  logger().info("stop {}", r);
  architecture->A.cleanup();
  logger().debug("cleanup");
  double c = (rand() % 4) * .01;
  architecture->A.core().C = c;
  logger().debug("A.C {}", c);
  r = architecture->A.configure();
  logger().debug("configure {}", r);
  r = architecture->A.get_task().activate();
  logger().debug("activate {}", r);
  r = architecture->A.get_task().start(clock, nullptr);
  logger().info("start {}", r);
  mauve::tracing::trace_end("reconfiguration");
}

extern "C" void mk_python_deployer() {
  mk_abstract_deployer(new ReconfigurationArchitecture());
}

}
