#r['/A']['N'],r['/A']['S0'],r['/A']['R0'],r['/A']['mean(E)'],r['/A']['min(E)'],r['/A']['max(E)'],r['/A']['stdev(E)'],r['/A']['mean(T)'],r['/A']['min(T)'],r['/A']['max(T)'],r['/A']['stdev(T)'],r['/B']['S0'],r['/B']['R0'],r['/B']['mean(E)'],r['/B']['min(E)'],r['/B']['max(E)'],r['/B']['stdev(E)'],r['/B']['mean(T)'],r['/B']['min(T)'],r['/B']['max(T)'],r['/B']['stdev(T)']
NR == 1 {
  min_S0_A = $2
  max_S0_A = $2
  min_S0_B = $12
  max_S0_B = $12
  min_R0_A = $3
  max_R0_A = $3
  min_R0_B = $13
  max_R0_B = $13
}
{
  sum_S0_A += $2;
  min_S0_A = (min_S0_A < $2 ? min_S0_A : $2)
  max_S0_A = (max_S0_A > $2 ? max_S0_A : $2)
  var_S0_A += $2^2;
  sum_S0_B += $12;
  min_S0_B = (min_S0_B < $12 ? min_S0_B : $12)
  max_S0_B = (max_S0_B > $12 ? max_S0_B : $12)
  var_S0_B += $12^2;
  sum_R0_A += $3;
  min_R0_A = (min_R0_A < $3 ? min_R0_A : $3)
  max_R0_A = (max_R0_A > $3 ? max_R0_A : $3)
  var_R0_A += $3^2;
  sum_R0_B += $13;
  min_R0_B = (min_R0_B < $13 ? min_R0_B : $13)
  max_R0_B = (max_R0_B > $13 ? max_R0_B : $13)
  var_R0_B += $13^2;
  sum_N += $1;
  sum_E_A += $1 * $4;
  sum_E_B += $1 * $14;
  sum_T_A += $1 * $8;
  sum_T_B += $1 * $18;
  var_E_A += ($1 - 1) * $7;
  var_E_B += ($1 - 1) * $17;
  var_T_A += ($1 - 1) * $11;
  var_T_B += ($1 - 1) * $21;
}
END {
  mean_S0_A = sum_S0_A/NR;
  mean_R0_A = sum_R0_A/NR;
  mean_S0_B = sum_S0_B/NR;
  mean_R0_B = sum_R0_B/NR;

  printf("%.3f & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f\n", \
    (sum_E_A/sum_N), sqrt(var_E_A / (sum_N - NR)), \
    (sum_E_B/sum_N), sqrt(var_E_B / (sum_N - NR)), \
    (sum_T_A/sum_N), sqrt(var_T_A / (sum_N - NR)), \
    (sum_T_B/sum_N), sqrt(var_T_B / (sum_N - NR)))

  printf("%.3f & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f & %.3f\n", \
    mean_S0_A, sqrt(var_S0_A/NR-(mean_S0_A)^2), \
    mean_S0_B, sqrt(var_S0_B/NR-(mean_S0_B)^2), \
    mean_R0_A, sqrt(var_R0_A/NR-(mean_R0_A)^2), \
    mean_R0_B, sqrt(var_R0_B/NR-(mean_R0_B)^2))
}
