import mauve_runtime
from rospkg import RosPack
rp = RosPack()
p = rp.get_path("benchmarking")
mauve_runtime.load_deployer(p + "/../../devel/lib/libmauve_cpu_reconf.so")
depl = mauve_runtime.deployer

mauve_runtime.initialize_logger(b'''
default:
  level: info
  type: stdout
''')

depl.architecture.configure()
depl.create_tasks()
depl.activate()

depl.start()
